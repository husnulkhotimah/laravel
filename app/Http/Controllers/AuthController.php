<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view("page.register");
    }

    public function welcome(Request $request)
    {
        $FirstName = $request['FirstName'];
        $LastName = $request['LastName'];

        return view('page.welcome', ['FirstName' => $FirstName, 'LastName' => $LastName]);
    }
}
