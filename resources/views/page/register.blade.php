@extends('layout.master')

@section('title')
SanberBook   
@endsection

@section('content')
    
<h2>Sign Up Form</h2>
<form action="/welcome" method="post">
    @csrf
    <label>First Name:</label><br>
    <input type="text" name="FirstName"><br><br>
    <label>Last Name:</label><br>
    <input type="text" name="LastName"><br><br>
    <label>Gender:</label><br>
    <input type="radio" name="GENDER">Male <br>
    <input type="radio" name="GENDER">Female <br>
    <input type="radio" name="GENDER">Other <br><br>
    <label>Nationality:</label>
    <select name="NATIONALITY"> <br>
        <option value="Indonesian">Indonesian</option>
        <option value="Malaysian">Malaysian</option>
        <option value="Australian">Australian</option>
        <option value="American">American</option>
        <option value="Belgian">Belgian</option> <br> <br>
    </select><br><br>
    <label>Language Spoken:</label> <br>
    <input type="checkbox"> Bahasa Indonesia <br>
    <input type="checkbox"> English <br>
    <input type="checkbox"> Other <br><br>
    <label>Bio:</label><br>
    <textarea name="Bio" id="" cols="30" rows="10"></textarea><br>
    <button>Sign Up</a> </button>
</form>
@endsection