<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');

Route::get('/data-tables', function(){
    return view('page.data-tables');
});

Route::get('/table', function(){
    return view('page.table');
});

//CRUD Cast

//Create Data Cast
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');

//Read Data Cast
Route::get('/cast', 'CastController@index');
Route::get('/cast/{cast_id}', 'CastController@show');

//Update Data Cast
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');

//Delete Data Cast
Route::delete('/cast/{cast_id}', 'CastController@destroy');